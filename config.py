from dataclasses import dataclass


@dataclass
class SimulationParameters:

    watershed: int
    event_date: str
    manning_n: float
    precipitation_coverage: float
    timestep_delta_s: float
    dem_cell_size_m: float
    infiltration_rate_mm_s: float = 0

    def get_flumes_filename(self):
        return f'watersheds/{self.watershed}/flumes.geojson'
    
    def get_rainfall_data_filename(self):
        return f'watersheds/{self.watershed}/rainfall_{self.event_date}.csv'

    def get_runoff_data_filename(self):
        return f'watersheds/{self.watershed}/runoff_{self.event_date}.csv'

    def __str__(self) -> str:
        return f"""
    Watershed: {self.watershed}
    Event: {self.event_date}
    Manning N: {self.manning_n}
    Precipitation coverage: {self.precipitation_coverage}
    Delta t: {self.timestep_delta_s}
    Infiltration Rate (mm/s): {self.infiltration_rate_mm_s}
        """
    def __repr__(self) -> str:
        return str(self)