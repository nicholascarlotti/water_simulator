import cupy as np
from matplotlib import pyplot as plt
import matplotlib.cm as cm
import time
from model import WaterFlowSimulator
from persistance import SimulationPersistance, WATER_LEVEL_HIST, OUT_DISCHARGE
import json
import rasterio as rio
import rasterio.mask
import progress.bar
from precipitation import Precipitation
from config import SimulationParameters



def plot_depth(depth):
    plt.imshow(
        depth, extent=(0, depth.shape[1] - 1, 0, depth.shape[0] - 1),
        cmap=cm.get_cmap('Reds'),
        origin='upper')
    plt.colorbar()
    plt.show()

def load_flumes_mask(params : SimulationParameters, dem):
    flumes_file = open(params.get_flumes_filename())
    flumes_data = json.loads(flumes_file.read())
    flumes_features = [feature['geometry'] for feature in flumes_data['features']]
    mask, _ = rio.mask.mask(dem, flumes_features)
    return np.array(mask.astype(np.bool8))[0, :, :]


def main():
    params = SimulationParameters(
        watershed=11, event_date='2005_8_22', manning_n=0.038,
        precipitation_coverage=1, timestep_delta_s=5e-1, dem_cell_size_m=10,
        infiltration_rate_mm_s=0.001)

    print(f'Loading simulation data:')
    print(params)

    dem = rio.open(f"watersheds/{params.watershed}/dem.tif")
    altitude_m = np.array(dem.read(1).astype('float32'))
    zero_mask = altitude_m == 0


    flumes_mask = load_flumes_mask(params, dem)

    water_level_m = np.zeros_like(altitude_m)
    water_level_m[zero_mask] = 0

    start = time.time()
    simulator = WaterFlowSimulator(
        altitude_m, water_level_m, params.manning_n, params.dem_cell_size_m, params.timestep_delta_s)
    data_logger = SimulationPersistance(params, simulator,
                                        outflow_water_mask=flumes_mask.get(),
                                        log_mask=WATER_LEVEL_HIST | OUT_DISCHARGE)

    precipitation = Precipitation()
    precipitation.create_cloud_mask(
        shape=simulator.water_level_m.shape, coverage=params.precipitation_coverage)
    precipitation.load_precipitation_data(params.get_rainfall_data_filename())

    iterations = int((precipitation.get_data_length_seconds() + 140*60
                    ) / params.timestep_delta_s)

    print(
        f'Simulating {params.timestep_delta_s*iterations}s @{params.timestep_delta_s}s resolution...')

    bar = progress.bar.Bar(
        'Simulation is running...', max=iterations,
        suffix='%(percent).1f%% - ETA: %(eta_td)ss')

    # Infiltration loss per iteration
    infiltration_rate_loss_per_iteration_m_it = (
        params.infiltration_rate_mm_s * params.timestep_delta_s) / 1000 # Convert to meters

    for i in range(iterations):
        precipitation.make_it_rain(simulator.water_level_m, params.timestep_delta_s,
                                i * params.timestep_delta_s, params.precipitation_coverage)

        simulator.step()
        # if (i + 1) % (3 / params.timestep_delta_s) == 0:
        #     data_logger.log_water_history()
        if (i + 1) % (5 / params.timestep_delta_s) == 0:
            data_logger.log_out_flow()
        simulator.water_level_m[zero_mask] = 0
        # simulator.water_level_m[flumes_mask] = 0

        # Ground infiltration rate
        simulator.water_level_m = np.maximum(
            simulator.water_level_m - infiltration_rate_loss_per_iteration_m_it, 0)
        bar.next()

    bar.finish()
    elapsed = time.time() - start
    print(f'Done in {elapsed}s')
    print(
        f'Simulation efficiency: x{(params.timestep_delta_s * iterations) / elapsed}')

    data_logger.save_data(iterations, event_start_time=precipitation.start_time)
    print(f'Output file: {data_logger.out_file_name}')

if __name__ == "__main__":
    main()
