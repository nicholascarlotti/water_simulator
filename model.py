from __future__ import nested_scopes
import cupy as np


class WaterFlowSimulator:

    def __init__(self,
                 altitude_m: np.ndarray,
                 initial_water_level_m: np.ndarray,
                 manning_n: float,
                 ds: float,
                 dt: float) -> None:
        self.altitude_m = altitude_m
        self.water_level_m = initial_water_level_m
        self.manning_n = manning_n
        self.ds = ds
        self.dt = dt
        self.obl_ds = np.sqrt(2) * self.ds
        self.neighborhood_count = 9.0
        n, m = self.water_level_m.shape
        self.down_right_roll_idx = np.roll(
            np.arange(n * m, dtype=np.int16).reshape((n, m)),
            (1, 1),
            axis=(0, 1))
        self.down_left_roll_idx = np.roll(
            np.arange(n * m, dtype=np.int16).reshape((n, m)),
            (1, -1),
            axis=(0, 1))
        self.mempool = np.get_default_memory_pool()
        self.total_in_use_memory = 0
        self.memsize = 8 * 1024**3
        self.hydraulic_radius_pow_cached = False
        self.__R = None
        self.last_discharge_m3_s = None

    def man(self, delta_level, hydraulic_radius):
        R = np.power(hydraulic_radius, 2/3)
        delta_sign = np.sign(delta_level)
        return (R * np.sqrt(np.abs(delta_level)) * delta_sign) / self.manning_n

    def __horizontal_step(
            self, water: np.ndarray, total_level: np.ndarray,
            hydraulic_radius: np.ndarray):
        left_rolled_level = np.zeros_like(total_level)
        left_rolled_level[:, :-1] = total_level[:, 1:]
        left_rolled_level[:, -1] = total_level[:, 0]
        delta_level = (total_level - left_rolled_level) / self.ds
        del left_rolled_level

        delta_level_neg_map = delta_level < 0
        neg_flow_compensated_radius = hydraulic_radius.copy()
        neg_flow_compensated_radius[:, :-1][delta_level_neg_map[:, :-1]
                                            ] = hydraulic_radius[:, 1:][delta_level_neg_map[:, :-1]]
        neg_flow_compensated_radius[:, -1][delta_level_neg_map[:, -1]
                                           ] = hydraulic_radius[:, 0][delta_level_neg_map[:, -1]]

        water_speed = self.dt / (self.ds / self.man(delta_level,
                                                    neg_flow_compensated_radius))
        water_speed[np.isinf(water_speed)] = 0

        left_flow = np.clip(water_speed, -1, 0)
        right_flow = np.clip(water_speed, 0, 1)

        right_giving = np.minimum(
            water, delta_level * right_flow) / self.neighborhood_count
        left_giving = np.minimum(
            water[:, 1:],
            (delta_level * left_flow)[:, : -1]) / self.neighborhood_count

        pos = np.zeros_like(water)
        neg = np.zeros_like(water)
        neg[:, :-1] -= right_giving[:, :-1]
        neg[:, 1:] -= left_giving
        pos[:, 1:] += right_giving[:, :-1]
        pos[:, :-1] += left_giving

        return pos, neg

    def __vertical_step(self, water, total_level, hydraulic_radius):
        up_rolled_level = np.zeros_like(total_level)
        up_rolled_level[:-1, :] = total_level[1:, :]
        up_rolled_level[-1, :] = total_level[0, :]
        delta_level = (total_level - up_rolled_level) / self.ds

        del up_rolled_level

        delta_level_neg_map = delta_level < 0
        neg_flow_compensated_radius = hydraulic_radius.copy()
        neg_flow_compensated_radius[:-1, :][delta_level_neg_map[:-1, :]
                                            ] = hydraulic_radius[1:, :][delta_level_neg_map[:-1, :]]
        neg_flow_compensated_radius[-1, :][delta_level_neg_map[-1, :]
                                           ] = hydraulic_radius[0, :][delta_level_neg_map[-1, :]]

        water_speed = self.dt / (self.ds / self.man(delta_level,
                                                    neg_flow_compensated_radius))
        water_speed[np.isinf(water_speed)] = 0

        up_flow_speed = np.clip(water_speed, -1, 0)
        down_flow_speed = np.clip(water_speed, 0, 1)

        up_giving = np.minimum(
            water[1:, :],
            (delta_level * up_flow_speed)[: -1, :]) / self.neighborhood_count
        down_giving = np.minimum(
            water, delta_level * down_flow_speed) / self.neighborhood_count
        # up_giving = (dv * up_flow_speed)[:-1, :] / self.neighborhood_count
        # down_giving = (dv * down_flow_speed) / self.neighborhood_count

        pos = np.zeros_like(water)
        neg = np.zeros_like(water)

        neg[:-1, :] -= down_giving[:-1, :]
        neg[1:, :] -= up_giving
        pos[1:, :] += down_giving[:-1, :]
        pos[:-1, :] += up_giving
        return pos, neg

    def __diag_1_step(self, water, total_level, hydraulic_radius):
        # TODO: I'm not using np.take here, i should check that this is correct
        diag_up_rolled_level = np.zeros_like(total_level)
        diag_up_rolled_level[:-1, :-1] = total_level[1:, 1:]
        diag_up_rolled_level[-1, :-1] = total_level[0, 1:]
        diag_up_rolled_level[:-1, -1] = total_level[1:, 0]
        diag_up_rolled_level[-1, -1] = total_level[0, 0]

        delta_level = (total_level - diag_up_rolled_level) / self.obl_ds
        del diag_up_rolled_level

        delta_level_neg_map = delta_level < 0
        neg_flow_compensated_radius = hydraulic_radius.copy()
        neg_flow_compensated_radius[:-1, :-1][delta_level_neg_map[:-1, :-1]
                                              ] = hydraulic_radius[1:, 1:][delta_level_neg_map[:-1, :-1]]
        neg_flow_compensated_radius[-1, :-1][delta_level_neg_map[-1, :-1]
                                             ] = hydraulic_radius[0, 1:][delta_level_neg_map[-1, :-1]]
        neg_flow_compensated_radius[:-1, -1][delta_level_neg_map[:-1, -1]
                                             ] = hydraulic_radius[1:, 0][delta_level_neg_map[:-1, -1]]
        neg_flow_compensated_radius[-1, -1][delta_level_neg_map[-1, -1]
                                            ] = hydraulic_radius[0, 0][delta_level_neg_map[-1, -1]]

        water_speed = self.dt / (self.obl_ds / self.man(delta_level,
                                                        neg_flow_compensated_radius))
        water_speed[np.isinf(water_speed)] = 0

        up_flow_speed = np.clip(water_speed, -1, 0)
        down_flow_speed = np.clip(water_speed, 0, 1)

        down_giving = np.minimum(
            water, delta_level * down_flow_speed) / self.neighborhood_count
        up_giving = np.minimum(water, np.take(
            delta_level * up_flow_speed, self.down_right_roll_idx)) / self.neighborhood_count

        pos = np.zeros_like(water)
        neg = np.zeros_like(water)
        neg[:-1, :-1] -= down_giving[:-1, :-1]
        neg[1:, 1:] -= up_giving[1:, 1:]
        pos[1:, 1:] += down_giving[:-1, :-1]
        pos[:-1, :-1] += up_giving[1:, 1:]

        return pos, neg

    def __diag_2_step(self, water, total_level, hydraulic_radius):
        # TODO: I'm not using np.take here, i should check that this is correct
        diag_up_rolled_level = np.zeros_like(total_level)
        diag_up_rolled_level[:-1, 1:] = total_level[1:, :-1]
        diag_up_rolled_level[:-1, 0] = total_level[1:, -1]
        diag_up_rolled_level[-1, 1:] = total_level[0, :-1]
        diag_up_rolled_level[-1, 0] = total_level[0, -1]

        delta_level = (total_level - diag_up_rolled_level) / self.obl_ds
        del diag_up_rolled_level

        delta_level_neg_map = delta_level < 0

        neg_flow_compensated_radius = hydraulic_radius.copy()
        neg_flow_compensated_radius[:-1, 1:][delta_level_neg_map[:-1, 1:]
                                             ] = hydraulic_radius[1:, :-1][delta_level_neg_map[:-1, 1:]]
        neg_flow_compensated_radius[:-1, 0][delta_level_neg_map[:-1, 0]
                                            ] = hydraulic_radius[1:, -1][delta_level_neg_map[:-1, 0]]
        neg_flow_compensated_radius[-1, 1:][delta_level_neg_map[-1, 1:]
                                            ] = hydraulic_radius[0, :-1][delta_level_neg_map[-1, 1:]]
        neg_flow_compensated_radius[-1, 0][delta_level_neg_map[-1, 0]
                                           ] = hydraulic_radius[0, -1][delta_level_neg_map[-1, 0]]

        water_speed = self.dt / (self.obl_ds / self.man(delta_level,
                                                        neg_flow_compensated_radius))
        water_speed[np.isinf(water_speed)] = 0

        up_flow_speed = np.clip(water_speed, -1, 0)
        down_flow_speed = np.clip(water_speed, 0, 1)

        down_giving = np.minimum(
            water, delta_level * down_flow_speed) / self.neighborhood_count
        up_giving = np.minimum(water, np.take(
            delta_level * up_flow_speed, self.down_left_roll_idx)) / self.neighborhood_count

        pos = np.zeros_like(water)
        neg = np.zeros_like(water)

        neg[:-1, 1:] -= down_giving[:-1, 1:]
        neg[1:, :-1] -= up_giving[1:, :-1]
        pos[1:, :-1] += down_giving[:-1, 1:]
        pos[:-1, 1:] += up_giving[1:, :-1]

        return pos, neg

    def step(self):
        total_level = self.water_level_m + self.altitude_m

        # Approximating radius with depth. Plus one to avoid zero terms
        # that would require us to shift the resulting matrix otherwise.
        # hydraulic_radius = self.water_level_m

        hydraulic_radius = (
            self.water_level_m * self.ds) / (self.ds + self.water_level_m * 2)

        # hydraulic_radius = self.water / (self.ds + (self.water / self.ds **2) * 2)

        pos_h, neg_h = self.__horizontal_step(
            self.water_level_m, total_level, hydraulic_radius)
        pos_v, neg_v = self.__vertical_step(
            self.water_level_m, total_level, hydraulic_radius)
        pos_d1, neg_d1 = self.__diag_1_step(
            self.water_level_m, total_level, hydraulic_radius)
        pos_d2, neg_d2 = self.__diag_2_step(
            self.water_level_m, total_level, hydraulic_radius)
        iteration_pos_flow = pos_h + pos_v
        iteration_pos_flow += pos_d1 + pos_d2

        discharge_m3_s = (iteration_pos_flow * (self.ds ** 2)) / self.dt
        self.water_level_m += iteration_pos_flow + neg_h + neg_v
        self.water_level_m += neg_d1 + neg_d2

        current_step_usage = self.mempool.total_bytes()
        if self.total_in_use_memory + current_step_usage * 2 > self.memsize:
            self.mempool.free_all_blocks()
            self.total_in_use_memory = 0
        else:
            self.total_in_use_memory += current_step_usage
        self.last_discharge_m3_s = discharge_m3_s
        return discharge_m3_s
