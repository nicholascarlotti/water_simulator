import csv
from datetime import time
from math import floor
import cupy as np


class Precipitation:

    def __init__(self, cloud_mask=None) -> None:
        self.cloud_mask = cloud_mask
        self.start_time = None
        self.elapsed = []
        self.precipitation = []

    def create_cloud_mask(self, shape, coverage=.3):
        mean = np.array(shape) / 2
        cov = np.array(shape) * 2 * np.eye(2)
        samples = floor(shape[0] * shape[1] * coverage)
        self.cloud_mask = np.floor(
            np.random.multivariate_normal(mean, cov, samples)).astype(
            np.uint32)

    def make_it_rain(self, water, dt, time, coverage):
        water += self.get_precipitation(time) * dt * coverage

    def get_precipitation(self, time) -> float:
        """
        :param time: _description_
        :type time: _type_
        :return: _description_
        :rtype: float
        """
        t = time
        i = 0
        while True:
            try:
                if t < self.elapsed[i]:
                    i -= 1
                    break
            except IndexError:
                i = len(self.elapsed) - 1
                break
            i += 1
        return self.precipitation[i]

    def load_precipitation_data(self, file):
        with open(file) as csvfile:
            self.elapsed = []
            self.precipitation = []
            reader = csv.DictReader(csvfile, delimiter=',')
            start_time = None
            for row in reader:
                if start_time == None:
                    hh, mm = list(map(int, row['Start Time'].split(":")))
                    start_time = time(hour = int(hh), minute= int(mm))
                    self.start_time = start_time
                self.elapsed.append(int(row['Elapsed Time']) * 60)
                self.precipitation.append(
                    (float(row['Rainfall Rate (mm/hr)']) / 1000) / 3600)  # Convert them to m/s

    def get_data_length_seconds(self):
        if len(self.elapsed) == 0:
            raise Exception("No data was loaded yet")
        return self.elapsed[-1]
