import numpy as np
from model import WaterFlowSimulator
from config import SimulationParameters
from datetime import datetime
from os import getpid

WATER_LEVEL_HIST = 0b000001
OUT_DISCHARGE = 0b000010
HORIZONTAL_VELOCITY = 0b000100
VERTICAL_VELOCITY = 0b001000
WATER_SPEED = 0b010000


class SimulationPersistance:

    def __init__(self, simulation_parameters: SimulationParameters,
                 simulator: WaterFlowSimulator, outflow_water_mask,
                 log_mask=WATER_LEVEL_HIST) -> None:
        self.simulator = simulator
        self.water_history_meters = []
        self.flume_runoff_m3_s = []
        self.precipitation_m_s = []
        self.out_file_name = f'{datetime.now().isoformat()}_{getpid()}'
        self.outflow_water_mask = outflow_water_mask
        self.logmask = log_mask
        self.simlation_parameters = simulation_parameters

    def log_step_data(self):
        if self.logmask & WATER_LEVEL_HIST > 0:
            self.log_water_history()
        if self.logmask & OUT_DISCHARGE > 0:
            self.log_out_flow()

    def log_water_history(self):
        self.water_history_meters.append(self.simulator.water_level_m.get())

    def log_out_flow(self):
        discharge = self.simulator.last_discharge_m3_s.get()
        masked_discharge = discharge[self.outflow_water_mask]
        discharge = masked_discharge.sum()
        self.flume_runoff_m3_s.append(discharge)

    def log_precipitation(self, value):
        self.precipitation_m_s.append(value)

    def save_data(self, total_iterations=None, **kwargs):
        np.savez_compressed(self.out_file_name,
                            water_history=self.water_history_meters,
                            flume_runoff_m3_s=self.flume_runoff_m3_s,
                            precipitation=self.precipitation_m_s,
                            iterations=total_iterations,
                            **vars(self.simlation_parameters),
                            **kwargs)
