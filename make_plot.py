from datetime import time, date, datetime
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
import csv
import argparse

from config import SimulationParameters
from precipitation import Precipitation

parser = argparse.ArgumentParser()
parser.add_argument('file', type=str)
args = vars(parser.parse_args())


data = np.load(args['file'], allow_pickle=True)
water_history = data['water_history']
iterations = data['iterations']
delta_time = data['timestep_delta_s']
delta_space = data['dem_cell_size_m']
out_flow = data['flume_runoff_m3_s']
precipitation_event_start_time: time = data['event_start_time'].item()
duration_seconds = iterations * delta_time
out_flow_time = np.arange(out_flow.shape[0], dtype=np.float32)
out_flow_time *= duration_seconds / out_flow.shape[0]

watershed = data['watershed']
event_date = data['event_date']

simulation_parameters = SimulationParameters(
    watershed=watershed, event_date=event_date, manning_n=0,
    precipitation_coverage=0, timestep_delta_s=delta_time,
    dem_cell_size_m=delta_space)
runoff_data_file = open(simulation_parameters.get_runoff_data_filename())
reader = csv.DictReader(runoff_data_file, delimiter=',')


precipitation = Precipitation()
precipitation.load_precipitation_data(simulation_parameters.get_rainfall_data_filename())


runoff_value = []
runoff_time = []
runoff_event_start_time: time = None
for row in reader:
    if runoff_event_start_time == None:
        hh, mm = list(map(int, row['Start Time'].split(":")))
        runoff_event_start_time = time(hour=hh, minute=mm)
    runoff_value.append(float(row['Runoff Rate (f3/s)']) / 35.315
                        )
    runoff_time.append(float(row['Elapsed Time']) * 60)

runoff_value = np.array(runoff_value)
runoff_time = np.array(runoff_time)
today = date.today()

# Runoff and rainfall events have a different starting time, the simulation starts at
# rainfall start but the available runoff data starts at a later time.
# Thus it's necessary to timewise align simulation results and runoff data.
runoff_time += (datetime.combine(today, runoff_event_start_time) - datetime.combine(
    today, precipitation_event_start_time)).total_seconds()


print("Loaded simulation with parameters:")
print(f"  Duration: {duration_seconds}s")
print(f"  Resolution: {delta_time}s")
print(f"  Cell size: {delta_space**2}m2")


plt.plot(out_flow_time, out_flow * 1000, label="Simulated event")
plt.plot(runoff_time, runoff_value, 'r', label = "Real event")
# plt.plot(precipitation.elapsed, np.array(precipitation.precipitation) * 1000000, 'c-')
plt.legend()
plt.show()
fig, ax = plt.subplots(constrained_layout=True)
ax.axis('off')
array_plot = ax.imshow(
    water_history[0],
    animated=True, vmin=0,
    vmax=water_history.max() * .2,
    extent=(0, water_history[0].shape[1] - 1, 0, water_history[0].shape[0] - 1))
cbar = plt.colorbar(array_plot)


def animate(i):
    array_plot.set_array(water_history[i])
    return [array_plot]


print("Generating animation...")
anim = animation.FuncAnimation(fig, animate, frames=len(water_history))
print("Saving video file...")
file_name = f'simulation_{duration_seconds}s@{delta_time}_{delta_space}m.mp4'
anim.save(file_name, fps=30, extra_args=['-vcodec', 'libx264'])
print(f"Output file: {file_name}")
