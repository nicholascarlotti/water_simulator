# Cellular automata water simulator

This project implements water flow simulation modeled as a cellular automata. It uses CUDA to accelarate computation and achieve low simulation time.

## Data setup

The simulator needs a DEM and a precipitation data stream file to work. This data has to be put into the `watersheds/` directory under a subfolder with the name of the specific scenario you want to simulate (you can name this folder however you want to).
These files are to follow these requirements:

- __DEM file__: Must be a TIFF file (or equivalent, has to work with rasterio) with the name `dem.tif`.
- __Precipitation data__: Must be a csv file matching the `rainfall_{YYYY}_{MM}_{DD}.csv` naming scheme. As for the format of the file, refer to the example files (the only fields needed by the simulator are `Start Time`, `Elapsed Time` and `Rainfall Rate (mm/hr)`).
- __Flumes file__: The simulator has to know where to measure water flow in order to collect data, a flume sensor has to be defined. To do this, a `flumes.geojson` file has to be put into the scenario subfolder, please refer to the existing example. Note that the flume position file is likely to be extracted via your GIS data editor. NOTE: __Only one flume sensor is supported by the simulator__. Though it should be fairly easy to make it possible to use multiple ones.

## Setup
Step 0 is to have a CUDA compatible GPU.  
First, install python requirements from the `requirements.txt` file.
[Cupy](https://cupy.dev/) is required, please refer to their guide on how to install. Note that you also have to install CUDA libraries.


## How to run

Make sure to set the correct parameters in the main file. A `SimulationParameters` object has to be set up with the parameters for the simulation you want to run. For example, if you want to run a scenario from watershed 11 that happened Aug 22nd, 2005 you shall set the following parameters:
```python
    params = SimulationParameters(
        watershed=11, # Here
        event_date='2005_8_22', # Here
        # Parameters below are scenario independent, set however you see fit
        manning_n=0.038,
        precipitation_coverage=1,
        timestep_delta_s=5e-1,
        dem_cell_size_m=10,
        infiltration_rate_mm_s=0.001)
```

Once the parameters are set, just run `python3 main.py`.
By default an output file gets generated (name is dynamic, will be printed in you terminal) containing the measured Q value from your flumes.
