---
author: |
  | Carlotti Nicholas - Matr.883229
date: "Università degli Studi di Milano-Bicocca, A.A. 2021/2022"
header-includes:
  - \usepackage[bottom]{footmisc}
  - \usepackage{hyperref}
  - \usepackage{cleveref}
  - \usepackage{xfrac}
  - \usepackage{caption}
  - \usepackage{subcaption}
toc: true
papersize: a4
fontsize: 12pt
geometry: margin=3cm
bibliography: citations.bib
lang: it
title: Simulatore di corsi fluviali tramite automa cellulare
abstract: |
  In questa relazione verrà presentato un simulatore per flussi di acqua superficiale. Il modello su cui si basa il simulatore è quello degli automi cellulari, che consentono di modellare fenomeni naturali con un approccio microscopico.
  Repository: https://gitlab.com/nicholascarlotti/water_simulator
---

\clearpage

# Introduzione
Con questo progetto viene presentato un modello per la simulazione di corsi d'acqua superficiali. Il modello è basato su un automa cellulare a celle quadrate. La scelta è stata guidata dal tipo di dato in ingresso, ovvero una __digital elevation map__ dell'area geografica su cui eseguire la simulazione.  
Nel cuore del modello vi è una formula empirica per il calcolo della velocità di un flusso d'acqua in un canale a pelo libero (ovvero un canale in cui la superfice superiore del liquido non è confinata), la __formula di Manning__. Questa formula, di uso molto diffuso nel campo dell'idraulica, è una espansione della __formula di Chézy__, che ha introdotto la possibilità di applicarla a un qualunque canale note le sue caratteristiche.  
Il progetto ha come obiettivo quello di implementare un modello simile a [@Parsons] e migliorarne diversi aspetti, in particolare la velocità di simulazione. Questo è stato ottenuto grazie ad una formulazione del problema che ne permettesse la risoluzione su hardware parallelo (nello specifico una GPU).


# Motivazione e stato dell'arte
Modellare il comportamento di un flusso d'acqua è un processo necessario nel campo del controllo delle alluvioni. Quando vengono progettate opere da collocare a ridosso di corsi d'acqua che tendono ad esondare, o in prossimità di zone costiere, viene valutato il rischio di inondazione e le possibili contromisure adottabili per mitigare il problema.
Con l'avvento del cambiamento climatico, fenomeni meteorologici di portata maggiore sono sempre più frequenti, soprattutto per quanto riguarda fenomeni di precipitazione [@nasa_climate] [@epa_climate_change], portando ad un conseguente aumento del numero di alluvioni in diverse aree del mondo.
Prevedere la portata e i danni di questi eventi diventa quindi necessità sempre più percepita.  
Negli anni diversi studi hanno presentato una metodologia per la prevenzione di disastri naturali da inondazione [@Lian2017], ed alcuni hanno anche condotto casi studio su città reali [@Cheng2017]. Simulare le dinamiche di un evento notevole può migliorare di molto l'efficacia delle tecniche di prevenzione.  
In letteratura sono presenti diverse proposte di modelli per simulare lo sviluppo di flussi d'acqua in seguito ad un evento precipitazione [@Parsons] [@Jamali]. Generalmente i modelli proposti si basano su automi cellulari, alcuni con regole di aggiornamento di una certa complessità [@bao2011lattice].  
Inoltre sono disponibili diversi tool professionali per la simulazione su ambientazioni arbitrarie di cui vegnono citati HEC-RAS[@HEC-RAS], sviluppato dal corpo ingegneri dell'esercito americano, e TUFLOW[@TUFLOW], un software commerciale. Questi sistemi si basano su modelli che risolvono diverse equazioni di flusso di liquidi portando a risultati molto precisi a scapito dei tempi di elaborazione. L'obiettivo di questo progetto è quindi quello di ottenere un simulatore sufficientemente preciso e dai ridotti tempi di calcolo.


# Implementazione
Il modello proposto da questo progetto è basato su un automa cellulare (CA), che sfrutta un'equazione empirica, nota come **formula di Manning** [@Manning] per la stima della velocità di un flusso d'acqua.  
Lo scopo principale del progetto è quello di reimplementare il modello proposto in [@Parsons] superandone le limitazioni principali, nello specifico la velocità di simulazione e la precisione dei risultati.  
L'automa sviluppato simula il comportamento del flusso d'acqua applicando la regola di update ad ogni iterazione. Le iterazioni corrispondono ad un passo in avanti nel tempo dell'evento simulato, questo passo è un parametro in ingresso al modello.
Ad ogni iterazione, per ogni cella, viene calcolato il livello di acqua da distribuire ai propri vicini usando un __vicinato di Moore__ di raggio 1. Il motivo per cui è stata scelta la distanza di Moore, rispetto a quella di Von-Neumann, è che questa permette di simulare meglio il comportamento di fenomeni di diffusione per quanto riguarda gli automi cellulari. Sebbene la struttura ideale sarebbe un automa cellulare con celle esagonali, purtroppo, data la natura dei dati in input (la digital elevation map), ottenere una griglia esagonale risulta difficile e porterebbe all'introduzione di approssimazioni aggiuntive che renderebbero potenzialmente inutili gli sforzi di questa trasformazione.

## Struttura dell'automa cellulare
La struttura del CA segue in modo naturale dal dato principale usato durante le simulazioni, la __digital elevation map__ (DEM) di una regione di territorio. Una DEM è una rappresentazione digitale dell'altitudine del terreno fornita in formato raster. Generalmente ogni pixel dell'immagine raster rappresenta un'area di 10m x 10m, ma esistono mappe a precisione maggiore.  
Data una DEM per una determinata area geografica, all'interno del modello viene creata una griglia di celle quadrate dove a ogni elemento verrà assegnato il valore di altitudine del DEM per la coordinata corrispondente a quella cella. La struttura così ottenuta è chiamata _altitudeGrid_ e contiene l'altitudine, in metri, di ogni cella del DEM.
Una volta ottenuta la griglia delle altitudini, ne viene creata una seconda che sarà utilizzata per gestire i valori del livello dell'acqua per ogni cella dell'_altitudeGrid_. Questa griglia verrà chiamata _waterGrid_ e ogni suo elemento conterrà il livello dell'acqua per quella cella in metri.  
Di ogni cella dell'automa abbiamo quindi due informazioni, legate all'altitudine del terreno ed al livello dell'acqua nella sua posizione.

### Regola di update {#sec:update-rule}
Come menzionato in precedenza, la regola di update si basa sulla formula di Manning [@Manning]. Questa formula viene utilizzata per calcolare la velocità di un flusso d'acqua che passa per un canale di cui sono note inclinazione e dimensioni. La formula è riportata qui di seguito:
$$V = \frac{k}{n} R_h^\frac{2}{3} S^\frac{1}{2}$$

I termini della formula sono:

- $R_h$: __Hydraulic radius__, è ottenuto calcolando il rapporto tra l'area per la quale vi è un flusso d'acqua e il perimetro del canale che viene bagnato dal flusso (si veda \cref{fig:hydraulic-radius}).
- $S$ (_slope_): L'inclinazione tra i due punti per i quali si vuole calcolare la velocità del flusso.
- $n$: Il coefficiente di Manning, è un valore calcolato empiricamente. È un indice della resistenza che il canale oppone al corso d'acqua.
- $k$: Costante di conversione nel caso in cui $n$ sia dato in unità imperiali.

![Hydraulic Radius (Fonte: A Level Geography^[https://www.alevelgeography.com/changing-channel-characteristics-cross-profile-wetted-perimeter-hydraulic-radius-etc/] \label{fig:hydraulic-radius})](./img/hydraulic-radius.jpg){width=300px}

Generalmente la formula viene usata per calcolare la velocità del flusso di acqua in un canale a cielo aperto, nel caso del progetto verrà però sfruttata per calcolare la velocità tra due celle adiacenti. Al fine di presentare come viene sfruttata la formula verrà illustrata un'approssimazione dell'algoritmo che calcola lo sviluppo del sistema ad ogni iterazione.

Innanzitutto definiamo alcuni termini:

- $dt$: È la durata, in tempo simulato, di un passo del modello.
- $ds$: È la distanza tra il centro di celle adiacenti (con vicinato di Von-Neumann)
- $ds_{diag}$: È la distanza tra il centro di celle adiacenti diagonalmente ($ds_{diag} = \sqrt{2}ds$ )
- $depth_{i,j} = waterGrid_{i,j} + altitudeGrid_{i,j}$ È l'altezza totale della cella $i,j$

Assumiamo per semplicità che si vuole calcolare il flusso di acqua dalla cella $(i,j)$ alla cella $(k,l)$ e che $depth_{i,j} > depth_{k,l}$. Il primo passo è quello di calcolare i termini della formula di Manning.  
La prima variabile calcolata è $R_h$, per farlo utilizziamo la formula illustrata in \cref{fig:hydraulic-radius} assumendo un canale delle stesse dimensioni del volume d'acqua presente nella cella $(i,j)$.

$$R_h[i,j]=\frac{waterGrid_{i,j}\cdot ds}{2 \cdot waterGrid_{i,j} + {ds}}$$

Più in generale, data una coppia di celle adiacenti, il modello calcola $R_h$ basandosi sulla cella che tra le due agisce come fonte, ovvero quella che ha una profondità complessiva maggiore, e che quindi finirebbe a propagare la sua acqua in eccesso alla vicina.

In seguito viene calcolata $S$:

$$S = \frac{depth_{i,j} - depth_{k,l}}{ds}$$

Per quanto riguarda i parametri $n$ e $k$, il primo è ricavato da tabelle come quella in figura \cref{fig:manning-n-table}, dal momento che il suo valore è sempre fornito in unità SI, non è necessario considerare il parametro $k$.

![Tabella valori del coefficiente di Gauckler–Manning \label{fig:manning-n-table}](./img/manning_n_table.jpg)

Noti i componenti della formula di Manning, possiamo ricavare la velocità $V$ del flusso da $(i,j)$ a $(k,l)$. A questo punto occorre ricordare che ogni iterazione della simulazione corrisponde al passare di un istante di durata $dt$ dello scenario reale. Tenuto presente questo fattore, vogliamo ricavare quanta acqua viene spostata da $(i,j)$ a $(k,l)$ in un singolo passo di simulazione.  
A tal fine, viene prima calcolato il numero di secondi necessari affinchè il flusso da $(i,j)$ finisca in $(k,l)$:

$$time\_needed=\frac{ds}{V}$$

In seguito, viene calcolato quante frazioni di un passo di simulazione vale questo tempo:

$$steps\_needed = \frac{dt}{time\_needed}$$

Infine, $steps\_needed$ viene usato per determinare quanto del livello presente in $waterGrid_{i,j}$ trasferire in $waterGrid_{k,l}$, semplicemente moltiplicando questo fattore per la differenza di livello tra le due celle:

$$min(steps\_needed \cdot (depth_{i,j} - depth_{k,l}), waterGrid_{i,j})$$

L'operazione di minimo è una delle due misure che vengono prese per evitare che una cella possa diffondere più acqua di quella che effettivamente detiene.  
L'operatore $min$ viene usato per far sì che nello scambio di acqua tra due celle non si ecceda l'acqua portata dalla cella ${i,j}$ alla cella ${k,l}$ rispetto al volume effettivamente presente in ${i,j}$.
Successivamente, il valore di acqua da trasferire viene diviso per il numero di celle presenti in un vicinato (9 celle). Questo ha lo scopo di impedire che una cella che dovesse, durante una specifica iterazione, assumere il ruolo di fonte per più di una delle sue vicine, possa distribuire più acqua di quella che effettivamente può propagare. Dividendo per 9 si assume che questa cella agisca da fonte per _tutte_ le sue vicine.
In generale andrebbe valutato, per ogni iterazione, quante celle adiacenti hanno effettivamente un livello inferiore alla cella corrente e utilizzare questo numero per compiere la divisione. Tuttavia il modello sviluppato esegue questa semplificazione allo scopo di alleviare il calcolo computazionale ad ogni iterazione.

## Problema come matrice e CuPy
Modellare un sistema come automa cellulare consente di creare modelli potenti con una difficoltà implementativa particolarmente ridotta. Infatti, definendo automi che a livello microscopico, ovvero quello delle singole celle, seguono semplici regole si può dare origine a simulatori piuttosto precisi e realistici. Il problema più grosso con la modellazione a CA è quello della performance. Infatti nella sua implementazione naive, una singola iterazione di simulazione su CA consiste in un ciclo che applica la regola di update ad ogni singolo elemento dell'automa.  
Una delle priorità cardine di questo progetto è quella di sviluppare un modello che sia computazionalmente efficiente e che consenta di simulare scenari in tempi brevi. Il primo passo per ottenere questo obiettivo è quello di vedere il problema da un punto di vista matriciale. Infatti, una formulazione matriciale si presta perfettamente all'utilizzo di operazioni macchina vettoriali, permettendo di eseguire la stessa operazione su più dati in un singolo passo.  
Naturalmente strutture come _altitudeGrid_ e _waterGrid_ godono di una naturale rappresentazione matriciale, l'aspetto più complesso è quello che concerne la definizione delle operazioni. Prendiamo per esempio il calcolo:

$$S = \frac{depth_{i,j} - depth_{k,l}}{ds}$$

Questa formula ci permette di ottenere l'inclinazione tra due celle che viene utilizzata nella formula di Manning. Facendo un'assunzione più forte rispetto all'esempio portato nella sezione \ref{sec:update-rule} \nameref{sec:update-rule}, poniamo il caso che la cella $(i,j)$ si trovi a sinistra della cella $(k,l)$. La formula può essere trasformata come segue:

$$S = \frac{depth - depth_{leftRolled}}{ds}$$

Dove $depth_{leftRolled}$ non è altro che la matrice $depth$ le cui colonne sono state traslate di una posizione a sinistra (in modo circolare al fine di mantenere le stesse dimensioni). Così facendo è possibile calcolare il flusso d'acqua che si sposta, globalmente, in senso orizzontale. Infatti con le relative accortezze è possibile calcolare anche i flussi diretti da destra a sinistra riconoscendo per quali celle il calcolo precedente da' risultato negativo.  
Questa operazione, detta __matrix roll__, è al cuore del funzionamento dell'automa. Nell'implementazione effettiva l'operazione viene compiuta usando opportune finestre sulle matrici in modo da evitare di spostare fisicamente le colonne della matrice in memoria (portando ad un notevole risparmio di risorse).  

Dopo aver individuato una formulazione vantaggiosa per il problema, è stato necessario trovate un modo per sfruttarla al meglio. La soluzione è stata quella di utilizzare __CuPy__, una libreria __Python__ che permette di eseguire calcoli complessi utilizzando la GPU come accelerazione hardware. L'interfaccia di __CuPy__ è pensata per essere altamente simile a quella di __NumPy__, una libreria per calcolo scientifico per Python (ne è di fatto una trasposizione su GPU).  
La scelta di utilizzare l'accelerazione hardware si è rivelata l'elemento chiave per rendere il modello estremamente performante, battendo le tempistiche del simulatore preso come riferimento.

\clearpage

# Risultati
Verranno ora presentati i risultati sperimentali raccolti col modello. In primo luogo verrà confrontato il modello ottenuto con quello di riferimento, sia in termini di precisione che in termini di performance. Successivamente verrà mostrata la sensitivity analysis condotta su alcuni parametri ed alcune simulazioni di scenari aggiuntivi.

## Valutazione qualitativa

Prima di trattare la validazione quantitativa del modello, verranno mostrate alcune immagini che forniscono un primo segno di buon funzionamento del sistema. In \cref{fig:1h-water-side-by-side} è illustrata la DEM dell'area utilizzata per il tuning del modello (chiamata __sub-watershed 11__), al suo fianco il livello dell'acqua all'interno dell'automa dopo un'ora di pioggia simulata. È evidente come l'acqua presente nella mappa abbia creato dei percorsi definiti.

![](img/subwatershed_11_pink.png){width=50%}
![](img/water_after_1h.png){width=50%}
\begin{figure}[!h]
\caption{Confronto tra DEM del bacino 11 e l'acqua presente nell'automa dopo 1h di tempo simulato.}
\label{fig:1h-water-side-by-side}
\end{figure}

Nella \cref{fig:1-h-water-overlap} viene mostrata una sovrapposizione delle immagini in \cref{fig:1h-water-side-by-side}. Da quest'immagine emerge in modo chiaro che i percorsi formati dall'acqua coincidono con le zone di depressione locale della DEM, e che quindi risultano verosimili. Questo risultato è un primo segnale positivo per quanto riguarda il processo di validazione del modello, suggerendo che le dinamiche del sistema simulato siano state catturate correttamente.

![Sovrapposizione del livello di acqua a 1h di simulazione e del DEM  \label{fig:1-h-water-overlap}](img/water_after_1h_overlap.png)

\clearpage

## Confronto col modello di riferimento
Il modello presentato in [@Parsons] ottiene risultati piuttosto verosimili con i dati reali dell'evento simulato. In particolare gli autori hanno calibrato il loro modello sulla base dei dati forniti dal __Walnut Gulch Experimental Watershed__ (WGEW) [@WGEW], un bacino sperimentale in Arizona dotato di una fitta rete di sensori per la rilevazione di precipitazioni e flussi d'acqua. Collegato a questo bacino esiste un progetto di open data per quanto riguarda le rilevazioni compiute su tutto il territorio, che fornisce anche una mappatura DEM dell'intera area. 

![Visione satellitare del WGEW](./img/wgew.jpg)

Gli autori della ricerca di riferimento hanno concentrato i loro sforzi su un evento di precipitazione notevole, verificatosi il 4 Agosto 1980. In quell'occasione sul bacino si abbattè un temporale con picchi di $213.36 \sfrac{mm}{h}$, causando flussi d'acqua dalla portata di circa $25 \sfrac{m^3}{s}$. In particolare è stata presa in esame una sotto-regione nord-orientale del bacino, il __subwatershed 11__. La misura che ha guidato il processo di tuning è il _water discharge_ (annotato col simbolo __$Q$__), che indica il volume d'acqua che passa per una determinata sezione di spazio al secondo, viene espresso in $\sfrac{m^3}{s}$. Nel bacino questa metrica è raccolta da un sensore apposito posto nella punta sud-occidentale della regione analizzata.

![DEM del subwatershed 11. Il triangolo rosso indica la posizione del sensore della portata di acqua.](./img/subwatershed_11.png)

Di conseguenza anche il modello qui proposto è stato tarato sullo stesso evento e sulla stessa area. Alla fine del processo di tuning i parametri ottimali ottenuti sono:

- Coefficiente di Manning: $0.038$
- Infiltration Rate: $0.001 \sfrac{mm}{h}$
- Delta time: $0.5s$

I parametri appena elencati danno origine all'andamento di Q illustrato in \cref{fig:tuned-model}. Come si può osservare, la simulazione prevede il picco di Q circa 5 minuti in anticipo rispetto alla realtà (più precisamente 5 minuti e 5 secondi), mentre per quanto riguarda il volume del picco, questo viene sottostimato di $1.3\sfrac{m^3}{s}$.  

![Andamento di Q dato il modello tarato \label{fig:tuned-model}](./img/tuned_sw11_1980.png)

\clearpage

Rispetto al modello di riferimento, che nella sua simulazione migliore ottiene un picco anticipato rispetto alla realtà di 17 minuti ed un valore di Q sovrastimato di circa $8 \sfrac{m^3}{s}$, il modello proposto si comporta sensibilmente meglio, con l'ulteriore vantaggio di una simulazione più veloce. Infatti per simulare l'intera durata dell'evento, 164 minuti, con un passo temporale di 0.5 secondi (19680 iterazioni), il modello impiega circa 77.5 secondi. Sebbene non venga citata una durata in modo esplicito, nel presentare il modello di riferimento gli autori accennano al fatto che le loro simulazioni arrivano a durare persino 6 ore, cosa che lascia intuire che il risultato raggiunto da questo progetto sia decisamente un miglioramento.

Per completezza viene presentato il risultato della simulazione con gli stessi parametri del modello di riferimento (in particolare la simulazione __numero 2__), che differiscono da quelli ottimali individuati da questo progetto (\cref{fig:paper-like}).

![Andamento di Q dati gli stessi parametri di Parsons et al. \label{fig:paper-like}](./img/paper_like_sim.png)

La simulazione 2 del modello proposto da [@Parsons] usa un coefficiente di Manning di $0.02$, il grafico di Q risultante è drasticamente differente dal dato reale e dalla simulazione condotta in questo progetto (figura \ref{fig:parsons-sims-side-by-side} a destra).

<!-- ![](./img/parsons_sim_1.jpg){width=50%}
![](./img/parsons_sim_2.jpg){width=50%} -->
\begin{figure}[!h]
  \begin{subfigure}[t]{0.5\textwidth}
    \includegraphics{./img/parsons_sim_1.jpg}
    \caption{Simulazione 1 in Parsons et al.}
    \label{fig:parsons-sim-1}
    
  \end{subfigure}
\hfill
  \begin{subfigure}[t]{0.5\textwidth}
    \includegraphics{./img/parsons_sim_2.jpg}
    \caption{Simulazione 2 in Parsons et al.}
    \label{fig:parsons-sim-2}

  \end{subfigure}
\caption{Confronto simulazioni del modello di riferimento}
\label{fig:parsons-sims-side-by-side}
\end{figure}

È invece curioso notare come, con questi parametri, si ottenga un risultato pressochè identico alla simulazione 1 del modello di riferimento, che usa un coefficiente di Manning di valore $0.01$.  
In questo caso, i risultati forniti dal modello coincidono fedelmente con quelli ottenuti dal lavoro citato: Il picco infatti si trova a 17:50 minuti in anticipo rispetto al dato reale e il suo valore è sovrastimato di $7.6$ metri.  
L'unica differenza rispetto al modello preso in esame è la maggiore continuità della curva prodotta, è infatti riconosciuto anche dagli autori che le letture delle loro simulazioni sembrano essere piuttosto rumorose e che potrebbero necessitare di un filtro passa-basso per ridurre le discontinuità.  
Inoltre, il coefficiente di Manning trovato da questo progetto, $0.038$, risulta essere più verosimile con la realtà rispetto a quello individuato da Parsons et al: Il valore da loro individuato, $0.01$, corrisponde a terreni molto lisci che sono incompatibili con il terreno presente nel bacino. Una possibile spiegazione è che il modello da loro presentato, sfruttando una distanza di Von-Neumann, necessiti di un parametro dal valore più basso per permettere al liquido di propagarsi più in fretta in modo da compensare la ridotta diffusione imposta da questo vicinato.

\clearpage

## Sensitivity analysis
Al fine di comprendere appieno il contributo di ogni parametro sulle dinamiche del modello, è stata eseguita una sensitivity analysis sui seguenti parametri:

- _Coefficiente di Manning_: Imposta la viscosità del liquido simulato
- _timestep_: La durata in tempo di simulazione di ogni iterazione
- _precipitation rate_: Regola l'intensità dell'evento di precipitazione
  
Le metriche prese come riferimento per valutare l'effetto dei singoli parametri sono il tempo del picco di __$Q$__ e il suo valore.

### Coefficiente di Manning
Il coefficiente di Manning è il parametro più importante per quanto riguarda il modello sviluppato. Le sue variazioni influiscono pesantemente sul risultato delle simulazioni: infatti a valori più piccoli corrisponde un flusso più fluido e quindi ad una diffusione più veloce dell'acqua. Per questo motivo, quello che ci si aspetta è che all'aumentare del coefficiente i picchi registrati nel grafico di Q siano sempre più distanti nel tempo rispetto all'inizio della simulazione.  

![Grafico di Q rispetto al variare del coefficiente di Gauckler-Manning \label{fig:SA-manning}](./img/SA_manning_Q.png)

I risultati confermano le aspettative, infatti è evidente come, in \cref{fig:SA-manning}, all'aumentare del coefficiente di Manning il momento in cui viene registrato il picco è sempre più posticipato nel tempo. Inoltre, si può osservare in \cref{fig:SA-manning-peak}, che il valore di picco di Q sembra avere un andamento quadratico discendente in funzione del valore del coefficiente.

![Valore massimo di Q rispetto al variare del coefficiente di Gauckler-Manning \label{fig:SA-manning-peak}](./img/SA_manning_peaks.png)

È interessante notare il comportamento di Q quando il coefficiente di Manning assume valore $0.01$: Il picco si verifica in modo molto aggressivo, sia per tempismo che per valore, per poi precipitare velocemente ed infine incrementare nuovamente. Questo comportamento ad onda si verifica in quanto, evidentemente, il basso valore del coefficiente permette ad una prima ondata di acqua di defluire attraverso le celle dove è presente il sensore di portata. Questo avviene prima che una seconda ondata possa "attaccarsi" alla prima e contribuirne alle misurazioni, venendo quindi registrata addirittura diverse ore dopo.


### Timestep
Dei parametri analizzati, il passo temporale di ogni iterazione è quello che presenta il comportamento più irregolare. In \cref{fig:SA-timestep} è possibile notare come il grafico di Q varia rispetto a diversi valori del parametro.  

![Grafico di Q rispetto al variare del parametro ds \label{fig:SA-timestep}](./img/SA_timestep.png)

Si può osservare che il valore del picco sale man mano che il timestep, partendo da frazioni di secondo, si avvicina a $1s$ per poi diminuire drasticamente.  
Il motivo della diminuzione è forse da attribuire al fatto che, con timestep maggiori, ad ogni iterazione il numero di celle che può propagare acqua è minore. Esperimenti condotti durante le prime fasi di sviluppo del modello hanno evidenziato l'esistenza di questo fenomeno. A supporto di questa teoria vi è il fatto che, per quanto riguarda la curva relativa alla simulazione con $timestep = 10s$, la sua coda destra ha un valore maggiore rispetto a tutti gli altri scenari, come se vi fosse un certo volume d'acqua che, molto più lentamente, viene propagato per tutta la mappa.
In \cref{fig:SA-timestep-peaks} si può osservare più chiaramente la variazione del valore del picco di Q in funzione del parametro analizzato.

![Valore del picco in funzione di $ds$ \label{fig:SA-timestep-peaks}](./img/SA_timestep_peaks.png)

\clearpage

### Precipitation rate
Questo parametro influisce sulla quantità di pioggia che cade su tutta la mappa simulata. È bene notare che nel modello, la pioggia è implementata come una caduta omogenea di acqua su tutta la mappa. Il parametro regola quindi la percentuale di pioggia, registrata durante l'evento reale, che cade sulle celle della simulazione.  
L'effetto della variazione di questo parametro è abbastanza intuibile e, difatti, i dati confermano ciò che si può immaginare. 

![Grafico di Q in funzione di _precipitation rate_ \label{fig:SA-precipitation}](./img/SA_precipitation.png)

\clearpage

### Scenari aggiuntivi
Al fine di valutare meglio la bontà del modello, sono state eseguite simulazioni su scenari diversi da quello usato per la taratura dei parametri.

#### 30 Luglio 1981
Uno degli scenari aggiuntivi simulati è un evento di precipitazione con un picco di $76.2 \sfrac{mm}{h}$. Una caratteristica interessante di questo scenario è che l'evento metereologico che lo ha generato si è verificato in 2 ondate. Questo ha fatto sì che anche le letture eseguite sul flusso d'acqua all'interno del bacino registrassero due picchi nettamente distinti.  
In \cref{fig:Q-1981} si può notare che, nonostante una grossa imprecisione per quanto riguarda il secondo picco, il modello riesce a catturare piuttosto fedelmente la dinamica simulata.  
Le discontinuità generate dalla simulazione sono in parte attribuibili all'approssimazione compiuta nel modellare la precipitazione che, si ricorda, è applicata in modo omogeneo su tutta la mappa.

![Confronto tra il valore di Q simulato e registrato per il fenomeno del 30 Luglio 1981\label{fig:Q-1981}](./img/additional_1981_7_30.png)

\clearpage

#### 22 Agosto 2005
Il secondo scenario aggiuntivo riguarda un fenomeno di precipitazione verificatosi nell'estate del 2005. In quell'occasione, sul bacino 11 è caduta pioggia con un picco di $160.02 \sfrac{mm}{h}$. L'aspetto interessante è la costanza di questo evento: infatti per ben 10 minuti la precipitazione media registrata è stata di circa $132.6 \sfrac{mm}{h}$.

![Confronto tra il valore di Q simulato e registrato per il fenomeno del 22 Agosto 2005 \label{fig:Q-2005}](./img/additional_2005_8_22.png)

In questo caso i risultati non sono per niente ottimali, il picco viene anticipato di 12:50 minuti e il suo valore sottostimato di 4 metri. Un possibile colpevole potrebbe essere il modo in cui sono state modellate le precipitazioni, che nei casi di pioggie di volume costante nel tempo, portano a difficoltà nel catturare il comportamento reale del sistema.

\clearpage

## Conclusioni

Con questo progetto è stato presentato un modello per la simulazione di eventi di precipitazione e dei corsi d'acqua formatisi di conseguenza. Il simulatore sviluppato riesce a emulare le dinamiche del sistema reale in modo fedele e numericamente migliore rispetto al modello preso come riferimento. I tempi di simulazione risultano ampiamente ridotti grazie all'utilizzo dell'accelerazione hardware e all'ottimizzazione delle operazioni eseguite dal software.  

Nonostante ciò risultano apparenti diversi limiti imposti dalle semplificazioni applicate durante la fase di progettazione. In particolare il modello di precipitazione è piuttosto semplicistico e necessiterebbe di essere riformulato in modo da riprodurre fedelmente il comportamento reale di un evento pluviale. Questo potrebbe essere realizzato creando delle nuvole di punti, piazzati in modo aleatorio secondo una qualche distribuzione, che ad ogni iterazione rilasciano pioggia sulle celle del terreno sottostanti. \
A supporto di questo possibile sviluppo vi è il fatto che per ogni bacino vi sono diversi sensori di precipitazione, sparsi in più zone, che possono aiutare a guidare la creazione di un modello più verosimile. Infatti si potrebbe fare in modo che nei dintorni dei sensori venga prodotto un volume di pioggia coerente con le misurazioni compiute in quell'area.   
Inoltre sarebbe desiderabile assegnare un coefficiente di Manning diverso per ogni cella dell'automa, a seconda del tipo di terreno presente. Il __Southwest Watershed Research Center__, l'ente che rilascia liberamente tutti i dati utilizzati per questo progetto, rende disponibili mappe inerenti alla vegetazione e alla conformazione del terreno per tutto il bacino. Questo permetterebbe di ottenere una buona approssimazione del coefficiente di Manning per ogni cella del CA, con la conseguenza di una simulazione più fedele alla realtà.


\clearpage